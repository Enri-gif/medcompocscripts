# Tillad Fil og printer deling for alle profiler
Set-NetFirewallRule -DisplayGroup "File And Printer Sharing" -Enabled False -Profile Any

# Opret Tenable brugerkonto
# Password
$Password = Read-Host -AsSecureString
# Brugernavn
$Brugernavn = Read-Host

New-LocalUser $Brugernavn -Password $Password -FullName $Brugernavn -Description "Tenable konto for credentialed checks"

Tiljøj brugerkontoen til administrator gruppen
Add-LocalGroupMember -Group "Administrators" -Member $Brugernavn

# Tjekke at porte 139 og porte 445 er åbne

$FWRule139 = netstat -aon | findstr 139
$FWRule445 = netstat -aon | findstr 445

if($FWRule139 -eq $null)
{
    New-NetFirewallRule -DisplayName "Allow Inbound Port 139 for Tnable credentialed check" -Direction Inbound -LocalPort 139 -Protocol TCP -Action Allow
}

if($FWRule445 -eq $null)
{
    New-NetFirewallRule -DisplayName "Allow Inbound Port 139 for Tnable credentialed check" -Direction Inbound -LocalPort 445 -Protocol TCP -Action Allow
}

# Sæt AutoShareWks til 1
New-ItemProperty -Name AutoShareWks -Path HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters -Type DWORD -Value 1

# Sluk UAC control
New-ItemProperty -Name LocalAccountPTokenFilterPolicy -Path HKLM:\SOFTWARE\MICROSOFT\Windows\CurrentVersion\Policies\System -Type DWORD -Value 1