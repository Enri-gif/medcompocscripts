$User = "Tenable"
try
{
    $Result = (Get-LocalUser -Name $User -ErrorAction Stop).Enabled
    try
    {
        if ($Result)
        {
            Disable-LocalUser -Name $User
        }
        else
        {
            Enable-LocalUser -Name $User
        }
    }
    catch
    {
        $_.Exception.Message #in case disable fails
    }
}
catch
{
    $_.Exception.Message #if user doesnt exist
}