# Tid for testing
$Time = (Get-Date) + (New-TimeSpan -Days 0 -Hours 0 -Minutes 00 -Seconds 5) | Get-Date -Format hh:mm

# scripten der skal køres her f.eks. -Execute 'foo.ps1'
$Action = New-ScheduledTaskAction ~\Kode\medcompocscripts\TenableKontoStyring.ps1
# hvornår den skal køres her f.eks. -Once -Daily -At '3:00 AM'
$Trigger = New-ScheduledTaskTrigger -Once -Daily -At $Time
# sikkerheds kontekts f.eks. -UserId 'DOMAIN\user' -RunLevel Highest
$Principal = New-ScheduledTaskPrincipal -UserId "LOCALSERVICE" -LogonType ServiceAccount
#indstillinger f.eks. -RunOnlyIfNetworkAvailable
$Settings = New-ScheduledTaskSettingsSet

$Task = New-ScheduledTask -Action $Action -Principal $Principal -Trigger $Trigger -Settings $Settings

Register-ScheduledTask -TaskName "Tenable kontostyring" -InputObject $Task